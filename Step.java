package com.company;

public class Step {

    private int row;
    private int col;
    private int stepMatrix[][];
    private int value;
    private int level;
    private int parent;
    private int child;
    private static int DIM = 6;

    public Step(int row, int col, int[][] mat, int level)
    {
        this.row = row;
        this.col = col;
        this.stepMatrix = new int[DIM][DIM];
        for(int i = 0; i < DIM; i++)
        {
            for(int j = 0; j < DIM; j++)
            {
                this.stepMatrix[i][j] = mat[i][j];
            }
        }
        this.level = level;
    }

    public Step()
    {    }

    public int getRow() {
        return row;
    }

    public void setRow(int row) {
        this.row = row;
    }

    public int getCol() {
        return col;
    }

    public void setCol(int col) {
        this.col = col;
    }

    public int[][] getMatrix() {
        return stepMatrix;
    }

    public void setStepMatrix(int[][] matrix) {
        this.stepMatrix = matrix;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public int getParent() {
        return parent;
    }

    public void setParent(int parent) {
        this.parent = parent;
    }

    public int getChild() {
        return child;
    }

    public void setChild(int child) {
        this.child = child;
    }

    public int putDisk(int diskColor)
    {
        int numberOfDisksChanged = 0;

        if(stepMatrix[row][col] != -1)
        {
            //System.out.println("Invalid move! Steps class");
            return -1;
        }

        stepMatrix[row][col] = diskColor;

        int diskPos = -1; //for storing the location of the disk in row/column towards the left/right/up/down

        //Checking if there's disk with same color to the RIGHT
        for(int c = col + 1; c < DIM && stepMatrix[row][c] != -1 && diskPos == -1; c++)
        {
            if(stepMatrix[row][c] == diskColor)
            {
                diskPos = c;
            }
        }
        //if disk is found to the right and there is other colored disk between them:
        if(diskPos != -1 && diskPos > col + 1)
        {
            for(int c = col + 1; c < diskPos; c++)
            {
                stepMatrix[row][c] = diskColor;
                numberOfDisksChanged++;
            }
        }

        //Checking if there's disk with same color to the LEFT
        diskPos = -1;
        for(int c = col-1; c >= 0 && stepMatrix[row][c] != -1 && diskPos == -1; c--)
        {
            if(stepMatrix[row][c] == diskColor)
            {
                diskPos = c;
            }
        }
        //if disk is found to the left and there is other colored disk between them:
        if(diskPos != -1 && diskPos < col - 1)
        {
            for(int c = col - 1; c > diskPos; c--)
            {
                stepMatrix[row][c] = diskColor;
                numberOfDisksChanged++;
            }
        }

        //Checking if there's disk with same color DOWNWARDS
        diskPos = -1;
        for(int r = row+1; r < DIM && stepMatrix[r][col] != -1 && diskPos == -1; r++)
        {
            if(stepMatrix[r][col] == diskColor)
            {
                diskPos = r;
            }
        }
        //if disk is found to the downwards and there is other colored disk between them:
        if(diskPos != -1 && diskPos > row + 1)
        {
            for(int r = row + 1; r < diskPos; r++)
            {
                stepMatrix[r][col] = diskColor;
                numberOfDisksChanged++;
            }
        }

        //Checking if there's disk with same color UPWARDS
        diskPos = -1;
        for(int r = row-1; r >= 0 && stepMatrix[r][col] != -1 && diskPos == -1; r--)
        {
            if(stepMatrix[r][col] == diskColor)
            {
                diskPos = r;
            }
        }
        //if disk is found to the upwards and there is other colored disk between them:
        if(diskPos != -1 && diskPos < row - 1)
        {
            for(int r = row - 1; r > diskPos; r--)
            {
                stepMatrix[r][col] = diskColor;
                numberOfDisksChanged++;
            }
        }

        //Checking if there's disk with same color UP TO THE LEFT
        diskPos = -1;
        int c = col -1;
        for(int r = row-1; c >= 0 && r >= 0 && stepMatrix[r][c] != -1 && diskPos == -1; r--)
        {
            if(stepMatrix[r][c] == diskColor)
            {
                diskPos = r;
            }
            c--;
        }
        //if disk is found and there is other colored disk between them:
        if(diskPos != -1 && diskPos < row - 1)
        {
            c = col -1;
            for(int r = row - 1; r > diskPos; r--)
            {
                stepMatrix[r][c] = diskColor;
                numberOfDisksChanged++;
                c--;
            }
        }

        //Checking if there's disk with same color UP TO THE RIGHT
        diskPos = -1;
        c = col + 1;
        for(int r = row-1; c < DIM && r >= 0 && stepMatrix[r][c] != -1 && diskPos == -1; r--)
        {
            if(stepMatrix[r][c] == diskColor)
            {
                diskPos = r;
            }
            c++;
        }
        //if disk is found and there is other colored disk between them:
        if(diskPos != -1 && diskPos < row - 1)
        {
            c = col + 1;
            for(int r = row - 1; r > diskPos; r--)
            {
                stepMatrix[r][c] = diskColor;
                numberOfDisksChanged++;
                c++;
            }
        }

        //Checking if there's disk with same color DOWN TO THE LEFT
        diskPos = -1;
        c = col - 1;
        for(int r = row+1; c >= 0 && r < DIM && stepMatrix[r][c] != -1 && diskPos == -1; r++)
        {
            if(stepMatrix[r][c] == diskColor)
            {
                diskPos = r;
            }
            c--;
        }
        //if disk is found and there is other colored disk between them:
        if(diskPos != -1 && diskPos > row + 1)
        {
            c = col - 1;
            for(int r = row + 1; r < diskPos; r++)
            {
                stepMatrix[r][c] = diskColor;
                numberOfDisksChanged++;
                c--;
            }
        }

        //Checking if there's disk with same color DOWN TO THE RIGHT
        diskPos = -1;
        c = col + 1;
        for(int r = row+1; c < DIM && r < DIM && stepMatrix[r][c] != -1 && diskPos == -1; r++)
        {
            if(stepMatrix[r][c] == diskColor)
            {
                diskPos = r;
            }
            c++;
        }
        //if disk is found and there is other colored disk between them:
        if(diskPos != -1 && diskPos > row + 1)
        {
            c = col + 1;
            for(int r = row + 1; r < diskPos; r++)
            {
                stepMatrix[r][c] = diskColor;
                numberOfDisksChanged++;
                c++;
            }
        }

        if(numberOfDisksChanged == 0)
        {
            stepMatrix[row][col] = -1;
        }

        return numberOfDisksChanged;
    }

    //funtion to print the matrix
    public void printMatrix()
    {
        for(int r = 0; r < DIM; r++)
        {
            for(int c = 0; c < DIM; c++)
            {
                System.out.print(stepMatrix[r][c] + " ");
            }
            System.out.println();
        }
        System.out.println();
    }

    @Override
    public String toString()
    {
        //printMatrix();
        return "Step{" + "row=" + row + ", col=" + col + ", value=" + value + ", parent=" + parent + ", level=" + level + '}';
    }
}
