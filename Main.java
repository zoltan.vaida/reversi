package com.company;

import java.util.ArrayList;
import java.util.Scanner;

import static java.lang.Math.abs;
import static java.lang.Math.max;
import static java.lang.Math.min;


public class Main {

    // User color: black (1)
    // User starts always
    // Computer color: white (0)
    static int counter;
    static int matrix[][];
    static ArrayList<Step> steps;
    private static int numberOfBlackDisks = 0, numberOfWhiteDisks = 0;
    private static int MAXLEVEL = 9;
    private static Step  mainMove;
    private static int DIM = 6;


    public static void main(String[] args) {

        counter = 0;
        matrix = new int[DIM][DIM];
        steps = new ArrayList<Step>();

        initializeBoard();
        printBoard();

        boolean isGameOver = false;
        while(!isGameOver)
        {
            int rowCol = readUsersMove();
            int row = rowCol / 10;
            int col = rowCol % 10;
            int numberOfDisksChanged = putDisk(row, col, 1);

            if (numberOfDisksChanged > 0) {
                printBoard();

                //Computer's move
                System.out.println("Please wait...");
                ComputerMove();

                //checking if game is over
                isGameOver = isFinished();
                if(isGameOver)
                {
                    int result = count(matrix);
                    if(result > 0)
                    {
                        System.out.println("You won! Black disks: " + numberOfBlackDisks + " White disks: " + numberOfWhiteDisks);
                    }
                    else if(result < 0)
                    {
                        System.out.println("The computer won! Black disks: " + numberOfBlackDisks + " White disks: " + numberOfWhiteDisks);
                    }
                    else
                    {
                        System.out.println("It's draw! Black disks: " + numberOfBlackDisks + " White disks: " + numberOfWhiteDisks);
                    }

                }
            }

        }


    }

    //funtion to print the playing board to console
    public static void printBoard()
    {
        int i=0, j=0;
        System.out.print("  ");
        for(i = 0; i < DIM; i++)
        {
            System.out.print(i+ " ");
        }
        System.out.println();
        System.out.print("  ");
        for(i = 0; i < DIM; i++)
        {
            System.out.print("_ ");
        }
        System.out.println();

        for(int r = 0; r < DIM; r++)
        {
            System.out.print(r+ "|");

            for(int c = 0; c < DIM; c++)
            {
                if(matrix[r][c] == -1)
                {
                    //System.out.print((char)0x2b1c);
                    System.out.print("  ");
                }
                else if(matrix[r][c] == 1)
                {
                    //System.out.print((char)0x25cb + " ");
                    System.out.print("B ");
                }
                else if(matrix[r][c] == 0)
                {
                    //System.out.print((char)0x25CF + " ");
                    System.out.print("W ");
                }

            }

            System.out.print("|");
            System.out.println();

            if(r == DIM-1)
            {
                System.out.print("  ");
                for(int k = 0; k < DIM; k++)
                {
                    System.out.print((char)0x200AF + " ");
                }
            }
        }
        System.out.println();
    }

    //function to initialize the board (puts down the first 4 disks)
    public static void initializeBoard()
    {
        for(int r = 0; r < DIM; r++)
        {
            for(int c = 0; c < DIM; c++)
            {
                matrix[r][c] = -1;
            }
        }
        matrix[(DIM/2)-1][(DIM/2)-1] = 0;
        matrix[(DIM/2)-1][DIM/2] = 1;
        matrix[DIM/2][(DIM/2)-1] = 1;
        matrix[DIM/2][DIM/2] = 0;
    }

    //function to read the user's move
    public static int readUsersMove()
    {
        System.out.println("Where would You like to put the disk?");
        Scanner myInput = new Scanner( System.in );
        int row = myInput.nextInt();
        int column = myInput.nextInt();

        return (row*10)+column;
    }

    //function to check if putting down the disk is possible and reversing the oponent's
    // disks, if there are any between the disk that was just put down and any other disk
    // belonging to the user
    public static int putDisk(int row, int col, int diskColor)
    {
        int numberOfDisksChanged = 0;

        // check if the user is putting a disk in a place that is not near an existing disk
//        if(matrix[row-1][col] == -1 && matrix[row-1][col+1] == -1 && matrix[row][col+1] == -1 && matrix[row+1][col+1] == -1
//        && matrix[row+1][col] == -1 && matrix[row+1][col-1] == -1 && matrix[row][col-1] == -1 && matrix[row-1][col-1] == -1)
//        {
//            System.out.println("Invalid move!");
//            return -1;
//        }

        if(matrix[row][col] != -1)
        {
            System.out.println("Invalid move!");
            return -1;
        }

        matrix[row][col] = diskColor;

        int diskPos = -1; //for storing the location of the disk in row/column towards the left/right/up/down

        //Checking if there's disk with same color to the RIGHT
        for(int c = col+1; c < DIM && matrix[row][c] != -1 && diskPos == -1; c++)
        {
            if(matrix[row][c] == diskColor)
            {
                diskPos = c;
            }
        }
        //if disk is found to the right and there is other colored disk between them:
        if(diskPos != -1 && diskPos > col + 1)
        {
            for(int c = col + 1; c < diskPos; c++)
            {
                matrix[row][c] = diskColor;
                numberOfDisksChanged++;
            }
        }

        //Checking if there's disk with same color to the LEFT
        diskPos = -1;
        for(int c = col-1; c >= 0 && matrix[row][c] != -1 && diskPos == -1; c--)
        {
            if(matrix[row][c] == diskColor)
            {
                diskPos = c;
            }
        }
        //if disk is found to the left and there is other colored disk between them:
        if(diskPos != -1 && diskPos < col - 1)
        {
            for(int c = col - 1; c > diskPos; c--)
            {
                matrix[row][c] = diskColor;
                numberOfDisksChanged++;
            }
        }

        //Checking if there's disk with same color DOWNWARDS
        diskPos = -1;
        for(int r = row + 1; r < DIM && matrix[r][col] != -1 && diskPos == -1; r++)
        {
            if(matrix[r][col] == diskColor)
            {
                diskPos = r;
            }
        }
        //if disk is found to the downwards and there is other colored disk between them:
        if(diskPos != -1 && diskPos > row + 1)
        {
            for(int r = row + 1; r < diskPos; r++)
            {
                matrix[r][col] = diskColor;
                numberOfDisksChanged++;
            }
        }

        //Checking if there's disk with same color UPWARDS
        diskPos = -1;
        for(int r = row-1; r >= 0 && matrix[r][col] != -1 && diskPos == -1; r--)
        {
            if(matrix[r][col] == diskColor)
            {
                diskPos = r;
            }
        }
        //if disk is found to the upwards and there is other colored disk between them:
        if(diskPos != -1 && diskPos < row - 1)
        {
            for(int r = row - 1; r > diskPos; r--)
            {
                matrix[r][col] = diskColor;
                numberOfDisksChanged++;
            }
        }

        //Checking if there's disk with same color UP TO THE LEFT
        diskPos = -1;
        int c = col -1;
        for(int r = row-1; c >= 0 && r >= 0 && matrix[r][c] != -1 && diskPos == -1; r--)
        {
            if(matrix[r][c] == diskColor)
            {
                diskPos = r;
            }
            c--;
        }
        //if disk is found and there is other colored disk between them:
        if(diskPos != -1 && diskPos < row - 1)
        {
            c = col -1;
            for(int r = row - 1; r > diskPos; r--)
            {
                matrix[r][c] = diskColor;
                numberOfDisksChanged++;
                c--;
            }
        }

        //Checking if there's disk with same color UP TO THE RIGHT
        diskPos = -1;
        c = col + 1;
        for(int r = row-1; c < DIM && r >= 0 && matrix[r][c] != -1 && diskPos == -1; r--)
        {
            if(matrix[r][c] == diskColor)
            {
                diskPos = r;
            }
            c++;
        }
        //if disk is found and there is other colored disk between them:
        if(diskPos != -1 && diskPos < row - 1)
        {
            c = col + 1;
            for(int r = row - 1; r > diskPos; r--)
            {
                matrix[r][c] = diskColor;
                numberOfDisksChanged++;
                c++;
            }
        }

        //Checking if there's disk with same color DOWN TO THE LEFT
        diskPos = -1;
        c = col - 1;
        for(int r = row+1; c >= 0 && r < DIM && matrix[r][c] != -1 && diskPos == -1; r++)
        {
            if(matrix[r][c] == diskColor)
            {
                diskPos = r;
            }
            c--;
        }
        //if disk is found and there is other colored disk between them:
        if(diskPos != -1 && diskPos > row + 1)
        {
            c = col - 1;
            for(int r = row + 1; r < diskPos; r++)
            {
                matrix[r][c] = diskColor;
                numberOfDisksChanged++;
                c--;
            }
        }

        //Checking if there's disk with same color DOWN TO THE RIGHT
        diskPos = -1;
        c = col + 1;
        for(int r = row + 1; c < DIM && r < DIM && matrix[r][c] != -1 && diskPos == -1; r++)
        {
            if(matrix[r][c] == diskColor)
            {
                diskPos = r;
            }
            c++;
        }
        //if disk is found and there is other colored disk between them:
        if(diskPos != -1 && diskPos > row + 1)
        {
            c = col + 1;
            for(int r = row + 1; r < diskPos; r++)
            {
                matrix[r][c] = diskColor;
                numberOfDisksChanged++;
                c++;
            }
        }

        if(numberOfDisksChanged == 0)
        {
            matrix[row][col] = -1;
        }


        return numberOfDisksChanged;
    }

    public static void ComputerMove()
    {
        steps.add(new Step(0, 0, matrix, 0));

        unFold(0, matrix, 0, 0); // szerintem ide diskColor 0 kell

        Step step = new Step();

        int max = -9999;
        int n = steps.size();

        for(int i = 1; i < n; i++)
        {
            //System.out.println(steps.get(i).toString());

            steps.get(i).setValue(alphaBeta(1, -9999, 9999, steps.get(i), 1)); // szerinte mide diskColor 1 kell

            if(max < steps.get(i).getValue())
            {
                step = steps.get(i);
                //System.out.println("position: " + i + " value: " + steps.get(i).getValue());
                max = steps.get(i).getValue();
            }
        }

        System.out.println("Computer's move:");
        putDisk(step.getRow(), step.getCol(), 0);
        printBoard();
        steps.clear();
    }

    public static void unFold(int parent, int [][]m, int diskColor, int level)
    {
        int child = 1;

        for(int i = 0; i < DIM; i++)
        {
            for(int j = 0; j < DIM; j++)
            {
                Step step = new Step(i, j, m, level + 1);
                if(step.putDisk(diskColor) > 0)
                {
                    step.setParent(parent);
                    step.setChild(child);
                    child++;
                    steps.add(step);
                }

                step = null;
            }
        }
    }

    public static int alphaBeta(int depth, int alpha, int beta, Step step, int diskColor)
    {
        if(depth == MAXLEVEL)
        {
            return count(step.getMatrix());
        }

        ArrayList<Step> childrens = unFoldArray(step.getMatrix(), diskColor);
        int n = childrens.size();

        if(depth % 2 == 1)
        {
            for(int i = 0; i < n; i++)
            {
                alpha = max(alpha, alphaBeta(depth+1, alpha, beta, childrens.get(i), 0));   // szerintem ide 0 kell
                if(alpha >= beta)
                {
                    mainMove = step;
                    return alpha;
                }
            }

            mainMove = step;
            return alpha;
        }
        else
        {
            for(int i = 0; i < n; i++)
            {
                beta = min(beta , alphaBeta(depth+1, alpha, beta, childrens.get(i), 1));    // szerintem ide 1 kell
                if(alpha >= beta)
                {
                    return beta;
                }
            }

            return beta;
        }
    }

    public static ArrayList unFoldArray(int[][] m, int diskColor)
    {
        int child = 1;

        ArrayList<Step> stepsArray = new ArrayList<>();

        for(int i = 0; i < DIM; i++)
        {
            for(int j = 0; j < DIM; j++)
            {
                Step step = new Step(i, j, m, 0);
                if(step.putDisk(diskColor) > 0)
                {
                    step.setChild(child);
                    child++;
                    stepsArray.add(step);
                }
            }
        }

        return stepsArray;
    }

    //function to count the difference between the white and the black disks
    public static int count(int [][] mat)
    {
        numberOfBlackDisks = 0;
        numberOfWhiteDisks = 0;

        for(int i = 0; i < DIM; i++)
        {
            for(int j = 0; j < DIM; j++)
            {
                if(matrix[i][j] == -1)
                {
                    if(mat[i][j] == 0)
                    {
                        numberOfBlackDisks++;
                    }
                    if(mat[i][j] == 1)
                    {
                        numberOfWhiteDisks++;
                    }
                }
            }
        }

        return numberOfBlackDisks - numberOfWhiteDisks;
    }

    //function to check if game is finished
    public static boolean isFinished()
    {
        int emptyPositionsOnBoard = 0;

        for(int i = 0; i < DIM; i++)
        {
            for(int j = 0; j < DIM; j++)
            {
                if(matrix[i][j] == -1)
                {
                    emptyPositionsOnBoard++;
                }
            }
        }

        int result = abs(count(matrix));

        if(emptyPositionsOnBoard < DIM && result > 32)
        {
            return true;
        }

        if(emptyPositionsOnBoard == 0)   //no empty field left on the board
        {
            return true;
        }

        return false;
    }

}


